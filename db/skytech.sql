-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 27, 2014 at 11:00 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `skytech`
--
CREATE DATABASE IF NOT EXISTS `skytech` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `skytech`;

-- --------------------------------------------------------

--
-- Table structure for table `party`
--

CREATE TABLE IF NOT EXISTS `party` (
  `PartyID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Phone1` varchar(12) NOT NULL,
  `Phone2` varchar(12) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Website` varchar(255) NOT NULL,
  PRIMARY KEY (`PartyID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `party`
--

INSERT INTO `party` (`PartyID`, `Name`, `Address`, `Phone1`, `Phone2`, `Email`, `Website`) VALUES
(7, 'cjvkc', 'aschvsjl', '27574', '547465', 'admin@musicwaale.com', 'acksbjh');

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE IF NOT EXISTS `voucher` (
  `VoucherID` int(11) NOT NULL AUTO_INCREMENT,
  `Party` varchar(100) NOT NULL,
  `VoucherDate` date NOT NULL,
  `Note` varchar(500) NOT NULL,
  `Category` varchar(50) NOT NULL,
  `Item` varchar(50) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Price` float NOT NULL,
  `Amount` float NOT NULL,
  `Tax` float NOT NULL,
  `Discount` float NOT NULL,
  PRIMARY KEY (`VoucherID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`VoucherID`, `Party`, `VoucherDate`, `Note`, `Category`, `Item`, `Quantity`, `Price`, `Amount`, `Tax`, `Discount`) VALUES
(1, '', '2014-11-27', 'note', 'ascda', 'asc', 2, 100, 200, 0, 0),
(2, '', '2014-11-27', 'ciausb', 'adsdd', 'asd', 2, 100, 200, 0, 0),
(3, 'undefined', '2014-11-27', 'notea', 'sca', 'ad', 2, 100, 200, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
