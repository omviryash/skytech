$(document).ready(function(){
  //$('#tbl_party').dataTable();
  //fillPartyGrid();
  //alert("ok");
  //calculateTotal();
});

// function getWastage(){
// 	//alert("ok");
// 	var getData = {
// 		kaarigar_id: $("#selectKaarigar").val()
// 	};
// 	$.ajax({
//     type:'POST',
//     url:'ajax/getWastage.php',
//     data: getData,
//     success: function(data){
// 		//alert(data);
//          if(data!="NO"){
//             $("#Wastage").val(data);
//          } else if(data=="NO") {
//             $("#Wastage").val("");
//          }
//     }
//   });
// }

function clearPartyTextFields()
{
    $("#Name").val("");
    $("#Address").val("");
    $("#Phone1").val("");
    $("#Phone2").val("");
    $("#Email").val("");
    $("#Website").val("");
}

function fillPartyGrid(){
  $.ajax({
    type:'POST',
    url:'ajax/party_view.php',
    success: function(data){
      //alert(data);
         if(data!="NO"){
            $("#partyTbody").html(data);
         } else if(data=="NO") {
            $("#partyTbody").html("");
         }
    }
  });
}

function insertParty(){
  var insertData = $("#party-insert").serialize();
  $.ajax({
    type:'POST',
    url:'ajax/party_insert.php',
    data: insertData,
    success: function(data){
         if(data=="YES"){
            fillPartyGrid();
            clearPartyTextFields();
         }else{
             alert("can't insert the row");
         }
    }
  })
}

function deleteParty(thisObj){
  // alert();
  var id = $(thisObj).closest("tr").find(".partyID").val();
  $.ajax({
    type:'POST',
    url: 'ajax/party_delete.php',
    data:{party_id:id},
    success: function(data){
      //alert(data);
         if(data=="YES"){
            $(thisObj).closest("tr").fadeOut().remove();
         }else{
             alert("can't delete the row");
         }
    }
  })
}

function calculateAmount() {
  var result1 = $("#tempQuantity").val() * $("#tempPrice").val();
  $("#amountTotal").val(result1);
  $("#tempAmount").val(result1);
  $("#quantityTotal").val("1");
  var result2 = parseFloat($("#amountTotal").val()) + ($("#amountTotal").val() * $("#Tax").val() / 100);
  $("#amountAfterTaxTotal").val(result2);
  var result3 = parseFloat($("#amountAfterTaxTotal").val()) - $("#discount").val();
  $("#finalAmount").val(result3);
}

function calculateTotal()
{
  var quantitySUM = 0;
  var amountSUM = 0;
  var amountAfterTax = 0;
  var finalSUM = 0;
  
  $("#voucherTbody tr").each(function(i,row){
      quantitySUM = parseFloat(quantitySUM) + parseFloat($(row).find(".quantity").html());
      amountSUM = parseFloat(amountSUM) + parseFloat($(row).find(".less").html());
      amountAfterTax = parseFloat(amountAfterTax) + parseFloat($(row).find(".intouch").html());
      finalSUM = parseFloat(finalSUM) + parseFloat($(row).find(".netweight").html());
  });

  
  $("#quantityTotal").html(quantitySUM);
  $("#amountTotal").html(amountSUM);
  $("#amountAfterTaxTotal").html(amountAfterTax);
  $("#finalTotal").html(finalSUM);
}

function fillGrid()
{
	//getWastage();
  var vParty = $("#party :selected").val();
  var vDate = $("#year").val() + "-" + $("#month").val() + "-" + $("#day").val();
  var vFunction = $("#Function").val();
  // alert("called");
  var insertData = {
    Date: vDate,
    Party: vParty,
    Function: vFunction
  };

  $.ajax({
    type:'POST',
    url:'ajax/view.php',
    data: insertData,
    success: function(data){
      //alert(data);
         if(data!="NO"){
            $("#transactionTbody").html(data);  
            calculateTotal();
         } else if(data=="NO") {
            $("#transactionTbody").html("");
			      calculateTotal();
         }
    }
  });
}

function clearTextFields()
{
    $("#Item").val("");
    $("#GrossWeight").val("");
    $("#Less").val("");
    $("#InTouch").val("");
    $("#NetWeight").val("");
    $("#Touch").val("");
    $("#Fine").val("");
}

function insertVoucher(){
  var inDate = $("#year").val() +"-"+ $("#month").val() +"-"+ $("#day").val();
  var vNote = $("#note").val();
  var vParty = $("#party :selected").val();
  //calculateTotal();
  var insertData = $("#form-insert").serialize() + "&Date=" + inDate + "&Note=" + vNote + "&Party=" + vParty;
  // alert(insertData);
  $.ajax({
    type:'POST',
    url:'ajax/insert.php',
    data: insertData,
    success: function(data){
      alert(data);
         if(data=="YES"){
            alert("success");
            // fillGrid();
            // calculateTotal();
            // clearTextFields();
         }else{
             alert("can't insert the row");
         }
    }
  })
}

function deleteTransaction(thisObj){
  var id = $(thisObj).closest("tr").find(".transactionID").val()
  //alert(id);
  $.ajax({
    type:'POST',
    url: 'ajax/delete.php',
    data:{transaction_id:id},
    success: function(data){
      //alert(data);
         if(data=="YES"){
            $(thisObj).closest("tr").fadeOut().remove();
            calculateTotal();
         }else{
             alert("can't delete the row");
         }
    }
  })
}