<?php
define('PATH', dirname(__FILE__));
require('header.php'); 
include_once('config.php');
?>
<script type="text/javascript">
$(document).ready(function(){
  fillVoucherGrid();
});
function fillVoucherGrid()
{
  $.ajax({
    type:'POST',
    url:'ajax/voucher_view.php',
    success: function(data){
      //alert(data);
         if(data!="NO"){
            $("#voucherTbody").html(data);
         } else if(data=="NO") {
            $("#voucherTbody").html("");
         }
    }
  });
}

function deleteVoucher(e)
{
	if(confirm('Are You Sure Delete Record!')){
		var postData = {VoucherID:$(e).attr('lang')};
		$.ajax({
		    type:'POST',
		    url:'ajax/voucher_delete.php',
		    data:postData,
		    success: function(data){
		      if(data =="YES"){
		      	$(e).closest('tr').remove()
		      }
		    }
		});
	}
}
</script>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <aside class="right-side strech"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Voucher</h1>
    </section>
    
<!-- Main content -->
    <section class="content">
    	<!-- success or Error Message Display -->
        <?php
        	$message = $_SESSION['msg'];
			$display = "Internal error occured.";
			if($message != ""){ if($message == "success"){ $display = "Voucher added successfully";}
        ?>
        <div class="alert alert-<?php echo $message; ?>">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $display; ?>
        </div>
        <?php $_SESSION['msg'] = ""; } ?>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Voucher List</h3>
              <!-- <a href="voucher.php" class="save btn btn-primary btn-sm" >Add</a> -->
              <button type="button" class="btn btn-primary" style="margin-top: 4px; margin-left: 5px;" onclick="window.location='voucher.php'">Add</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding table-scroll">
              <table class="table table-striped" id="tbl_party">
                <thead>
                  <tr>
                    <th>Voucher Type</th>
                    <th>Party Name</th>
                    <th>Date</th>
                    <th>Note</th>
                    <th>Tax</th>
                    <th>After Tax</th>
                    <th>Discount</th>
                    <th>Final Total</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="voucherTbody">
                  
                </tbody>
              </table>
            </div>
            <!-- /.box-body --> 
          </div>
        </div>
      </div>
    </section>
    <!-- /.content --> 
  </aside>
  <!-- /.right-side --> 
</div>
<!-- ./wrapper --> 
<?php require('footer.php'); ?>