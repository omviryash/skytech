<?php
define('PATH', dirname(__FILE__));
require('header.php'); 
include_once('config.php');
include_once CLASSPATH.'party.class.php';

$party = new Party();
$party_res = $party->view();
//var_dump($party_res);exit;

?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <aside class="right-side strech"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Party</h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12"> 
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Add new Party</h3>
            </div>
            <!-- /.box-header --> 
            <!-- form start -->
            <form role="form" method="post" action="" id="party-insert">
              <div class="box-body">
                <div class="form-group">
                  <label>Name:</label>
                  <input type="text" class="form-control x-large" name="Name" id="Name">
                </div>
                <div class="form-group">
                  <label>Address:</label>
                  <input type="text" class="form-control x-large" name="Address" id="Address">
                </div>
                <div class="form-group">
                  <label>Phone1:</label>
                  <input type="text" class="form-control x-large" name="Phone1" id="Phone1">
                </div>
                <div class="form-group">
                  <label>Phone2:</label>
                  <input type="text" class="form-control x-large" name="Phone2" id="Phone2">
                </div>
                <div class="form-group">
                  <label>Email:</label>
                  <input type="text" class="form-control x-large" name="Email" id="Email">
                </div>
                <div class="form-group">
                  <label>Website:</label>
                  <input type="text" class="form-control x-large" name="Website" id="Website">
                </div>
              </div>
              <!-- /.box-body -->
              
              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="insertParty();" name="Submit">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box --> 
          
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Party List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding table-scroll">
              <table class="table table-striped" id="tbl_party">
                <thead>
                  <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone1</th>
                    <th>Phone2</th>
                    <th>Email</th>
                    <th>Website</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="partyTbody">
                  
                </tbody>
              </table>
            </div>
            <!-- /.box-body --> 
          </div>
        </div>
      </div>
    </section>
    <!-- /.content --> 
  </aside>
  <!-- /.right-side --> 
</div>
<!-- ./wrapper --> 
<?php require('footer.php'); ?>