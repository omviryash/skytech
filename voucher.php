<?php
define('PATH', dirname(__FILE__));
include_once('config.php');

require('classes/party.class.php');
require('classes/voucher.class.php');

$party = new Party();
$voucher = new Voucher();

if($_POST)
{
	$voucher_post['VoucherID'] = $_POST['VoucherID'];
	$voucher_post['Type'] = $_POST['Function'];
	$voucher_post['PartyID'] = $_POST['PartyID'];
	$voucher_post['VoucherDate'] = $_POST['year']."-".$_POST['month']."-".$_POST['day'];
	$voucher_post['Note'] = $_POST['Note'];
	$voucher_post['Tax'] = $_POST['Tax'];
	$voucher_post['AmountAfterTax'] = $_POST['amountAfterTaxTotal'];
	$voucher_post['Discount'] = $_POST['Discount'];
	$voucher_post['FinalTotal'] = $_POST['finalAmount'];
	if($_POST['VoucherID'] == ""){
		$voucherID = $voucher->insertVoucher($voucher_post);
	}else if($_POST['VoucherID'] > 0)
	{
		$voucherID = $_POST['VoucherID'];
		$voucher->updateVoucher($voucher_post);
	}
	$vdetail_post['VoucherID'] = $voucherID;
	$vdetail_post['Category'] = $_POST['Category'];
	$vdetail_post['Item'] = $_POST['Item'];
	$vdetail_post['Qty'] = $_POST['Quantity'];
	$vdetail_post['Price'] = $_POST['Price'];
	$vdetail_post['Amount'] = $_POST['Amount'];
	$voucherDetailID = $voucher->insertVoucherDetail($vdetail_post);
	$message = "danger";
	if($voucherDetailID > 0 && $voucherDetailID > 0){
		$message = "success";
	}
	$_SESSION['msg'] = $message;
	Redirect("voucherlist.php");
}
function Redirect($url){
	header ("Location: $url");
}
require('header.php'); 
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <aside class="right-side strech"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Voucher</h1>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12"> 
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Entry</h3>
            </div>
            <!-- /.box-header --> 
            <!-- form start -->
            <form role="form" name="voucheForm" method="post" action="">
            	<input type="hidden" name="VoucherID" id="VoucherID" />
              <div class="box-body">
                <div class="form-group">
                  <label>Type:</label>
                  <select class="form-control medium" name="Function" id="Function">
                  	<option value="Purchase">Purchase</option>
                    <option value="Sales">Sales</option>
                    <option value="Sales Return">Sales Return</option>
                    <option value="Purchase Return">Purchase Return</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Party:</label>
                  <!-- <input list="party" class="form-control  x-small"> -->
                  <select name="PartyID" class="form-control medium" id="party">
                  	<option>Select a party</option>
                    <?php
                        $result = $party->getAllParty();
                        while($row = mysql_fetch_assoc($result)) {
                            echo "<option value=".$row['PartyID']." ";
                            if(isset($_SESSION['kaarigar_id'])){if($_SESSION['kaarigar_id'] == $row['PartyID']){echo "selected";}}
                            echo ">".$row['Name']."</option>";
                        }
                    ?>
                  </select>
                  </select>
                </div>
                <div class="form-group">
                  <label>Date :</label>
                  <div class="input-inline">
                   <?php $day=date('d'); $month=date('m'); $year=date('Y'); ?>
                    <input type="text" maxlength="2" class="form-control  x-small" placeholder="Day" name="day" id="day" value="<?php echo $day; ?>">
                    <input type="text" maxlength="2" class="form-control  x-small" placeholder="Month" name="month" id="month" value="<?php echo $month; ?>">
                    <input type="text" maxlength="4" class="form-control  x-small" placeholder="Year" name="year" id="year" value="<?php echo $year; ?>"></div>
                </div>
                <div class="form-group">
                  <label>Note :</label>
                 	<textarea name="Note" cols="" rows="" id="note" class="form-control medium"></textarea>
                </div>
              </div>
              <div class="box-footer">
              	<!-- <a href="voucherlist.php" class="save btn btn-danger btn-sm" >Back</a> -->
              	<button type="button" class="btn btn-danger" style="margin-top: 4px; margin-left: 5px;" onclick="window.location='voucherlist.php'">Back</button>
              </div>
              <!-- /.box-body -->
            <!-- </form> -->
              <!-- <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="voucherFillGrid();">Submit</button>
              </div>
            </form> -->
          </div>
          <!-- /.box --> 
          
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Vouchers</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding table-scroll">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th width="15">Sr.No.</th>
                    <th>Category</th>
                    <th>Item</th>
                    <th class="text-right">Qty.</th>
                    <th class="text-right">Price</th>
                    <th class="text-right">Amount</th>
                    <th class="action">Action</th>
                  </tr>
                </thead>
                <tbody id="voucherTbody">
                  <!-- <tr>
                    <td>1.</td>
                    <td>Category 1</td>
                    <td>Item 1</td>
                    <td class="text-right"> 100</td>
                    <td class="text-right">2</td>
                    <td class="text-right">200</td>
                    <td class="action">
                    	<a href="javascript:;" class="edit"><i class="fa fa-pencil"></i></a><a href="javascript:;" class="delete"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr> -->
                </tbody>
                  <tfoot>
                  <!-- <form method="post" action="" id="form-insert"> -->
                   <tr>
                    <td></td>
                    <td><input type="text" class="form-control small" name="Category"></td>
                    <td><input type="text" class="form-control small" name="Item"></td>
                    <td class="text-right"><input type="text" class="form-control x-small pull-right" id="tempQuantity" name="Quantity"></td>
                    <td class="text-right"><input type="text" class="form-control x-small pull-right" name="Price" id="tempPrice" onblur="calculateAmount();"></td>
                    <td class="text-right"><input type="text" class="form-control x-small pull-right" id="tempAmount" name="Amount" readonly="readonly" ></td>
                    <td class="action">
                    </td>
                  </tr>
                  
                  <tr>
                    <td colspan="7"> &nbsp;</td>
                  </tr>
                  <tr>
                    <td class="text-right" colspan="3"><strong>Total</strong></td>
                    <td class="text-right"><input type="text" class="form-control x-small pull-right" id="quantityTotal" readonly="readonly"></td>
                    <td class="text-right" colspan="2" name="Amount"><input type="text" class="form-control x-small pull-right" id="amountTotal" readonly="readonly"></td>
                    <td class="action">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-right" colspan="5"><strong>Tax %</strong></td>
                    <td class="text-right"><input type="text" class="form-control x-small pull-right" name="Tax" id="Tax" value="0" onblur="calculateAmount();"></td>
                    <td class="action">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-right" colspan="5"><strong>Amount after Tax</strong></td>
                    <td class="text-right" colspan=""><input type="text" name="amountAfterTaxTotal" class="form-control x-small pull-right" readonly="readonly" id="amountAfterTaxTotal"></td>
                    <td class="action">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-right" colspan="5"><strong>Discount</strong></td>
                    <td class="text-right"><input type="text" class="form-control x-small pull-right" name="Discount" value="0" id="discount" onblur="calculateAmount();";></td>
                    <td class="action">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-right" colspan="5"><strong>Final Amount</strong></td>
                    <td class="text-right" colspan=""><input name="finalAmount" type="text" class="form-control x-small pull-right" readonly="readonly" id="finalAmount"></td>
                    <td class="action">
                      <!-- <a href="#" class="save btn btn-primary btn-sm" onclick="insertVoucher(this);">Save</a> -->
                      <button type="submit" class="btn btn-primary" >Submit</button>
                    </td>
                  </tr>
                <!-- </form> -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body --> 
          </div>
        </div>
      </div>
    </section>
    <!-- /.content --> 
  </aside>
  <!-- /.right-side --> 
</div>
<!-- ./wrapper --> 

<?php require('footer.php'); ?>